function createNewUser() {
    return {
        firstName: prompt('Enter your name'),
        lastName: prompt('Enter your surname'),
        birthday: prompt('Enter your date of birth in format "dd.mm.yyyy"')
            .split('.').reverse().join('.'),

        getLogin: function () {
            let index = this.firstName.slice(0, 1);
            let login = index + this.lastName;
            return login.toLowerCase()
        },

        getAge: function () {
            let now = new Date();
            let birthday = new Date(this.birthday);
            let diffInDays = Math.floor((now.getTime() - birthday.getTime()) / (1000 * 60 * 60 * 24));
            return Math.floor(diffInDays / 365);
        },

        getPassword: function () {
            let index = this.firstName.slice(0, 1);
            let lastNameInLow = this.lastName.toLowerCase()
            let year = this.birthday.split('.')[0];

            return index + lastNameInLow + year
        },
        setFirstName: function (newFirstName) {
            Object.defineProperty(
                this,
                'firstName',
                {
                    value: newFirstName,
                    writable: false
                }
            );
        },
        setLastName: function (newLastName) {
            Object.defineProperty(
                this,
                'lastName',
                {
                    value: newLastName,
                    writable: false
                }
            );
        },
        getFirstName: function () {
            return this.firstName;
        },
        getLastName: function () {
            return this.lastName;
        },
    }
}

let newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

newUser.setFirstName('Vasia')
newUser.setLastName('Pupkin')
console.log(newUser);

newUser.firstName = 'Petrik';
newUser.lastName = 'Pyatochkin';
console.log(newUser);


