let flexWrapHeader = document.querySelector('.flex-wrap_header');
let div = document.createElement('div');
let funnyThemeClass = 'funny-theme';
let button = document.createElement('button');
button.classList.add('button-theme');
button.textContent = `Change the theme`;

div.append(button);
flexWrapHeader.prepend(div);


let wrapFlex = document.querySelector('.flex-wrap');
let wrapGrid = document.querySelector('.grid-wrap');
let wrapFlexPosts = document.querySelector('.flex-wrap_posts');
let flexHeading = document.querySelector('.flex_heading');
let gridHeadings = document.querySelectorAll('.grid_heading');
let gridWrapFooter = document.querySelector('.grid-wrap_footer');


button.addEventListener('click', changeTheme);

init();

function changeTheme() {
    button.style.outline = 'none';
    button.classList.toggle(funnyThemeClass);
    wrapFlex.classList.toggle(funnyThemeClass);
    wrapGrid.classList.toggle(funnyThemeClass);
    wrapFlexPosts.classList.toggle(funnyThemeClass);
    flexHeading.classList.toggle(funnyThemeClass);
    for (let gridHeading of gridHeadings) {
        gridHeading.classList.toggle(funnyThemeClass)
    }
    flexWrapHeader.classList.toggle(funnyThemeClass);
    gridWrapFooter.classList.toggle(funnyThemeClass);
    localStorage.setItem(funnyThemeClass, isFunnyThemeEnabled())
}

function isFunnyThemeEnabled() {
    return button.classList.contains(funnyThemeClass) ? '1' : '0';
}

function init() {
    let funnyThemeValue = parseInt(localStorage.getItem(funnyThemeClass));
    if (!isNaN(funnyThemeValue) && funnyThemeValue > 0) {
        button.click();
    }
}