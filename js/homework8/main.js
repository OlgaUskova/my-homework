let form = document.createElement('form');
form.classList.add('form-price');
form.style.cssText = `
    padding: 20px;
    width: 250px;
    background: #ea3c53;
`;

let label = document.createElement('label');
label.textContent = 'Price, $';
label.style.cssText = `
    color: white;
    font-size: 28px;
    font-wight: bold;
    padding-right: 10px;
`;

let input = document.createElement('input');
input.setAttribute('type', 'number');
input.classList.add('price');
input.placeholder = 'Enter price...';
input.style.cssText = `
    width: 140px;
    height: 40px;
    border-radius: 5px;
    border: none;
    font-size: 18px;
    padding-left: 8px;
`;

document.querySelector('body').append(form);
form.append(label);
form.append(input);

// on focus
document.querySelector('.price').addEventListener('focus', function (event) {
    event.target.style.border = '2px solid green';
    event.target.style.outline = 'none';
});

// on blur
document.querySelector('.price').addEventListener('blur', function (event) {
    let value = event.target.value;

    if (parseFloat(value) < 0) {
        showError(event);
        removeInformer(event);
        event.target.style.border = '2px solid red';
        input.value = '';
    } else if (parseFloat(value) >= 0) {
        showInformer(value);
        removeError(event);
        event.target.style.border = '2px solid green';
    } else {
        removeError(event);
        removeInformer(event);
        event.target.style.border = 'none';
    }
});

document.body.addEventListener('click', function (event) {
    if (event.target.id === 'close-btn') {
        event.target.closest('.informer').remove();
        input.value = '';
    }
});

let showInformer = function (value) {
    let informer = document.querySelector('.informer');

    if (informer === null) {
        informer = createInformer();
    }
    informer.innerText = `Current price: ${value}`;
    informer.append(createCloseBtn());
};

let removeInformer = function () {
    let closeBtn = document.querySelector('.close');

    if (closeBtn !== null) {
        closeBtn.click();
    }
};

let createInformer = function () {
    let informer = document.createElement('span');

    informer.classList.add('informer');
    informer.style.cssText = `
        margin-bottom: 10px;
        display: inline-block;
        border: 1px solid #D3D3D3;
        border-radius: 100px;
        padding: 7px 20px;
        font-size: 20px;
        color: #A9A9A9;
    `;
    document.querySelector('.form-price').before(informer);
    return informer;
};

let createCloseBtn = function () {
    let closeButton = document.createElement('button');

    closeButton.innerText = 'X';
    closeButton.setAttribute('id', 'close-btn');
    closeButton.classList.add('close');
    closeButton.style.cssText = `

        border: 1px solid #A9A9A9;
        padding: 7px 10px;
        border-radius: 50px;
        color: #A9A9A9;
        background: white;
        position: relative;
        left: 10px;
    `;
    return closeButton;
};

let showError = function (event) {
    event.target.style.borderColor = 'red';

    let errorElement = document.querySelector('.error');

    if (errorElement === null) {
        errorElement = document.createElement('span');
        errorElement.classList.add('error');
        errorElement.innerText = 'Please enter correct price';
        errorElement.style.cssText = `
            position: relative;
            top: 10px;
         `
    }
    event.target.closest('.form-price').append(errorElement);
};

let removeError = function () {
    let errorElement = document.querySelector('.error');

    if (errorElement !== null) {
        errorElement.remove();
    }
};