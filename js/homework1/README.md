###1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

 var. Переменая var является глобальной переменной, соответственно ее видимость не ограничивается блоком, даже если
      она задана внутри блока.
      Переменная может быть объявлена дважды и это не приведет к ошибке, в отличие от let

 let. При объявлении переменной через let, значение данной переменной может быть изменено.
      При объявлении переменной внутри блока (фигурных скобок), она видна тольков в пределах этого блока.
      Для того чтобы переменная была видна, она должна быть объявлена.

 const. При объявлении переменной через const, значение данной переменной не может быть изменено, const следует
        использовать только втом случае, если нам известно, что значение переменной не будет перезаписано.


###2. Почему объявлять переменную через var считается плохим тоном?

  При объявлении переменной через var, ее область видимости становится слишком большой, в случае повторного
  объявления переменной с отличным значением от первоначального, может привести к перезаписи значения переменной,
  что приведет к ошибкам.
  Также, что немаловажно переменная var, объявленая за пределами какой-либо функции  будет доступна  даже для функций,
  объявленных в других файлах, подключённых к странице.