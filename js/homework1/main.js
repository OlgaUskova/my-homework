let userName;

do {
    userName = prompt('Please enter your name', '').trim();

    if (userName.length < 2) {
        alert('Invalid value. Please enter your name.');
    }
} while (userName.length < 2);
console.log(userName);

let userAge;

do {
    userAge = +prompt('Please enter your age', "");

    if (Number.isNaN(userAge)) {
        alert('Invalid value. Please enter your age');
    }
} while (Number.isNaN(userAge));
console.log(userAge);

if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if ( /*userAge >= 18 && */ userAge <= 22) {
    const response = confirm('Are you sure you want to continue?');
    if (response) {
        alert('Welcome, ' + userName);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert('Welcome, ' + userName);
}