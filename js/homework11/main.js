let buttons = document.querySelectorAll('.btn');
let codeLastBtn = '';

for (let button of buttons) {

    if (button.textContent === 'Enter') {
        button.dataset.code = button.textContent
    } else {
        button.dataset.code = `Key${button.textContent}`;
    }
}

document.addEventListener("keyup", highlightBtn);

function highlightBtn(event) {

    if (codeLastBtn !== '') {
        let lastBtn = document.querySelector('[data-code="' + codeLastBtn + '"]');

        lastBtn.style.background = 'black'
    }

    for (let button of buttons) {
        if (button.dataset.code === event.code) {
            button.style.background = 'blue';
            codeLastBtn = event.code;
        }
    }
}