function filterBy(array, type) {
    return array.filter((elem) => {
        return !(typeof elem === type);
    });
}
let result = filterBy(['hello', false, 'world', 23, '23', null, undefined, '', true], 'string');
console.log(result);