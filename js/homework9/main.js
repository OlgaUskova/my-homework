let tabs = document.querySelector(".tabs");
let tabsContent = document.querySelector(".tabs-content");

function start() {
    for (let i = 0; i < tabs.children.length; i++) {
        tabs.children[i].dataset.id = `${i + 1}`;
    }
    for (let i = 0; i < tabsContent.children.length; i++) {
        tabsContent.children[i].dataset.id = `${i + 1}`;

        if (i === 0) {
            tabsContent.children[i].classList.add('active')
        } else {
            tabsContent.children[i].style.display = 'none'
        }
    }
}
start();

tabs.addEventListener("click", showTab);

function showTab(event) {
    let className = "active";
    let tab = event.target.closest("li");

    // set active tab
    for (const li of tabs.children) {
        li.classList.remove(className);
    }
    tab.classList.add(className);

    // show content tab
    for (const li of tabsContent.children) {
        if (li.dataset.id === tab.dataset.id) {
            li.style.display = 'block';
        } else {
            li.style.display = 'none';
        }
    }
}