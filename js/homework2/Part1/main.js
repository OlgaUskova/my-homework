let number;

do {
    number = +prompt('Enter the number', "");

    if (!Number.isInteger(number)) {
        alert('Invalid value. Please enter an integer');
    }
} while (!Number.isInteger(number));


let flag = false;

for (let i = 1; i <= number; i++) {
    if (i % 5 === 0) {
        console.log(i);
        flag = true;
    }
}
if (flag === false) {
    console.log('Sorry, no numbers');
}