let m;
do {
m = +prompt('Enter first number',"");

    if (!Number.isInteger(m)) {
        alert('Invalid value. Please enter an integer');
    }
} while (!Number.isInteger(m));

let n;
do {
    n = +prompt('Enter second number that is greater than the first one',"");
    if(n<=m) {
        alert('Try again, the second number must be greater than the first one ');
    } else if(!Number.isInteger(n)) {
        alert('Invalid value. Please enter an integer');
    }
} while (n<=m || !Number.isInteger(n));

for (let i = m; i<=n; i++) {
    let isSimple = true;

    for (let j = 2; j<i; j++) {
        if (i % j === 0) {
            isSimple = false;
        }
    }
    if (isSimple) {
        console.log(i);
    }
}