$(function ($) {
    let flexWrapHeader = $(".flex-wrap_header");
    let links = '.links';
    let button = $('#scroll-btn');
    let section = $('.grid-wrap_content-2');
    let buttonCollapse = $('.collapse-button');


    $(flexWrapHeader).after('<div class="wrap-links"></div>');
    $('.wrap-links').append('<ul class="links"></ul>');
    $(links).append('<li class="list-link"><a class="link" href="#heading1">The latest news on design & architecture</a></li>');
    $(links).append('<li class="list-link"><a class="link"href="#heading2">Most popular posts</a></li>');
    $(links).append('<li class="list-link"><a class="link"href="#heading3">Our most popular clients</a></li>');
    $(links).append('<li class="list-link"><a class="link"href="#heading4">Top rated</a></li>');
    $(links).append('<li class="list-link"><a class="link"href="#heading5">Hot news</a></li>');

    $(".link").css({"text-decoration": "none", "color": "white"});
    $(".wrap-links").css({"background-color": "#14b9d5", "height": "50px"});
    $(links).css({
        "display": "flex", "justify-content": "space-evenly", "list-style": "none", "padding-top": "10px",
        "font-size": "20px"
    });


    button.on('click', getScroll);

    $(window).on('scroll', function () {
        scroll();
    });

    buttonCollapse.on('click', collapseSection);

    $('a[href*="#"]').on('click.smoothscroll', function (e) {
        smoothScroll(e);
    });


    function getScroll() {
        $(document.scrollingElement).animate({scrollTop: 0}, 500)
    }

    function scroll() {
        let windowHeight = $('window').height();
        let windowScroll = $('window').scrollTop();
        if (windowHeight < windowScroll) {
            button.fadeIn(200);
        } else {
            button.fadeIn(300);
        }
    }

    function collapseSection() {
        $(section).toggle();
        $('.collapse-button').text('Show section "HOT NEWS"')
    }

    function smoothScroll(e) {
        let hash = e.target.hash,
            _hash = hash.replace(/#/, ''),
            theHref = $(e.target).attr('href').replace(/#.*/, '');

        if (theHref && location.href.replace(/#.*/, '') != theHref) return;

        let $target = _hash === '' ? $('body') : $(hash + ', a[name="' + _hash + '"]').first();

        if (!$target.length) return;
        e.preventDefault();
        $('html, body').stop().animate({scrollTop: $target.offset().top - 0}, 400, 'swing', function () {
            window.location.hash = hash;
        });
    }
});
