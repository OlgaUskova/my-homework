let images = document.querySelectorAll('.image-to-show');
let current = 0;
let limit = Object.keys(images).length;
let id;
let buttonStop = document.createElement('button');
buttonStop.textContent = 'Stop showing';
buttonStop.style.cssText = `
    display: none;
    font-size: 24px;
    font-wight: bold;
    padding-right: 5px;
    margin-top: 10px;
`;
let buttonShow = document.createElement('button');
buttonShow.textContent = 'Resume showing';
buttonShow.style.cssText = `
    display: none;
    font-size: 24px;
    font-wight: bold;
    padding-right: 5px;
    margin-top: 10px;
`;
document.body.append(buttonStop);
document.body.append(buttonShow);

buttonStop.addEventListener('click', stopShowingImages);

buttonShow.addEventListener('click', startShowingImages);

init();

function init() {
    showImage(current);
    current++;
    startShowingImages();
}

function showImage(index) {
    for (let i = 0; i < limit; i++) {
        if (index === i) {
            images[i].style.display = 'block';
            buttonStop.style.display = 'block';
        } else {
            images[i].style.display = 'none';
        }
    }
}

function startShowingImages() {
    id = setInterval(function id() {
        showImage(current);
        current++;
        if (current >= limit) {
            current = 0;
        }
    }, 2000);
    buttonStop.style.display = 'block';
    buttonShow.style.display = 'none';
}

function stopShowingImages() {
    clearInterval(id);
    buttonStop.style.display = 'none';
    buttonShow.style.display = 'block';
}
