let div = document.createElement('div');
div.classList.add("content");
document.body.append(div);

let ul = document.createElement('ul');
ul.classList.add("most-visited-countries");
div.append(ul);

let button = document.createElement('button');
button.classList.add('cleaner');
button.textContent = 'Clear page content';
div.append(button);

addListItems(['France', 'Spain', 'USA', 'China', 'Italy', 'Turkey', 'Mexico', 'Thailand']);

button.addEventListener('click', function () {
    countdown(10);
});

function addListItems(arr) {

    arr.map((elem) => {
        let li = document.createElement("li");
        ul.appendChild(li);
        li.textContent = elem;
    });
}

function countdown(seconds) {
    let p = document.querySelector('.wrap-counter');

    if (p === null) {
        p = document.createElement('p');
        p.classList.add("wrap-counter");
        div.append(p);
    }

    if (seconds > 0) {
        p.innerHTML = `Page content will clear after <span class="counter">${seconds}</span> seconds`;
        p.style.cssText = `
            font-size: 20px;
        `;
        let span = p.querySelector('.counter');
        span.style.cssText = `
            font-size: 30px;
            font-weight: bold;
            color: red;
        `;
        seconds--;
        setTimeout(() => countdown(seconds), 1000);
    } else div.remove();
}