let form = document.querySelector('.password-form');
let icons = document.querySelectorAll('.fas');
let button = document.querySelector('.btn');
let inputs = document.querySelectorAll('input');

button.addEventListener('click', function (event) {
    let value = '';

    for (let input of inputs) {
        if (value === '') {
            value = input.value;
        } else {
            if (value !== input.value) {
                let error = document.querySelector(".error");
                if (error === null) {
                    error = document.createElement('span');
                    error.classList.add('error');
                    error.textContent = 'You need to enter the same values';
                    error.style.cssText = `
                        color: red;
                        position: relative;
                        bottom: 20px;
                        left: 103px;
                         `;
                    button.before(error);
                }
            } else {
               let error= document.querySelector(".error");
               if(error!==null) {
                   error.parentNode.removeChild(error);
               }
                setTimeout("alert('You are welcome')", 500);
            }
        }
    }
});

for (let icon of icons) {

    let label = icon.closest('label'); //
    let input = label.querySelector('input');

    icon.addEventListener('click', function () {

        let type = input.getAttribute('type');

        // change input type
        // change icon
        if (type === 'password') {
            input.setAttribute('type', 'text');
            icon.classList.remove('fa-eye');
            icon.classList.add('fa-eye-slash');
        } else if (type === 'text') {
            input.setAttribute('type', 'password');
            icon.classList.remove('fa-eye-slash');
            icon.classList.add('fa-eye');
        }
    });
}