function test(number1, number2, mathSign) {

    switch (mathSign) {
        case '+' :
            return number1 + number2;
        case '-' :
            return number1 - number2;
        case '*' :
            return number1 * number2;
        case '/' :
            return number1 / number2;
        default:
            alert('Unknown sign, enter math sign +, -, *, /')
    }
}

function getNumber(order='any') {

    let result;
    do {
        result = +prompt('Please enter the ' + order + ' number', "");

        if (Number.isNaN(result)) {
            alert('Invalid value. Please enter number');
        }
    } while (Number.isNaN(result));
    return result;
}

/**
 * @return {boolean}
 */
function IsMathSign(mathSign) {
    switch (mathSign) {
        case '+' :
        case '-' :
        case '*' :
        case '/' :
            return true;
        default:
            return false;
    }
}

function getMathSign() {
    let sign;
    do {
        sign = prompt('Enter math sign +, -, *, /', '');
        if (!IsMathSign(sign)) {
            alert('Unknown sign, enter math sign +, -, *, /');
        }
    } while (!IsMathSign(sign));
    return sign;
}


let number1 = getNumber('first');
let mathSign = getMathSign();
let number2 = getNumber('second');

let result = test(number1, number2, mathSign);
console.log(number1 + mathSign + number2 + '=' + result);