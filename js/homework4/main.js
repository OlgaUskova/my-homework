function createNewUser() {
    return {
        firstName: prompt('Enter your name'),
        lastName: prompt('Enter your surname'),

        getLogin: function () {
            let index = this.firstName.slice(0, 1);
            let login = index + this.lastName;
            return login.toLowerCase()
        },
        setFirstName: function (newFirstName) {
            Object.defineProperty(
                newUser,
                'firstName',
                {
                    value: newFirstName,
                    writable: false
                }
            );

        },
        setLastName: function (newLastName) {
            Object.defineProperty(
                newUser,
                'lastName',
                {
                    value: newLastName,
                    writable: false
                }
            );
        },
        getFirstName: function () {
            return this.firstName;
        },
        getLastName: function () {
            return this.lastName;
        },
    }
}

let newUser = createNewUser();
console.log(newUser.getLogin());

newUser.setFirstName('Vasia')
newUser.setLastName('Pupkin')
console.log(newUser);

newUser.firstName = 'Petrik';
newUser.lastName = 'Pyatochkin';
console.log(newUser);